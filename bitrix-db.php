<?php
try {
if (! @include_once( $_SERVER["DOCUMENT_ROOT"].'bitrix/php_interface/dbconn.php' ))
  throw new Exception ('Can\'t find dbconn.php! Script must be called from root bitrix site.');
}
catch(Exception $e) {    
  echo "\033[31m" . $e->getMessage() . "\033[0m \n";
  die;
}

$dumpname = "dump.sql";

function FileSizeConvert($bytes)
{
    $bytes = floatval($bytes);
        $arBytes = array(
            0 => array(
                "UNIT" => "TB",
                "VALUE" => pow(1024, 4)
            ),
            1 => array(
                "UNIT" => "GB",
                "VALUE" => pow(1024, 3)
            ),
            2 => array(
                "UNIT" => "MB",
                "VALUE" => pow(1024, 2)
            ),
            3 => array(
                "UNIT" => "KB",
                "VALUE" => 1024
            ),
            4 => array(
                "UNIT" => "B",
                "VALUE" => 1
            ),
        );

    foreach($arBytes as $arItem)
    {
        if($bytes >= $arItem["VALUE"])
        {
            $result = $bytes / $arItem["VALUE"];
            $result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
            break;
        }
    }
    return $result;
}

function mysqldump($dumpname) {
	global $DBHost;
	global $DBLogin;
	global $DBPassword;
	global $DBName;	
	return exec('mysqldump --user='.$DBLogin.' --password='.$DBPassword.' --host='.$DBHost.' '.$DBName.' > '.$dumpname);
}

function mysqlupload($dumpname) {
	global $DBHost;
	global $DBLogin;
	global $DBPassword;
	global $DBName;	
	return exec('mysql --user='.$DBLogin.' --password='.$DBPassword.' --host='.$DBHost.' '.$DBName.' < '.$dumpname);
}

function update_file_name($file) {
	$exist = false;
	$i = 0;
	global $dumpname;
	while(!$exist){
		if(file_exists($file.".".$i)){
			$i++;
			continue;
		} else {
			$exist = true;
			$file = $file.".".$i;
			rename($dumpname, $file);
		}		
	}
	return $file;
}

function remove($file) {
	echo "\033[1;33mDelete ".$file."? y/n\033[0m \n";
	$handle = fopen ("php://stdin","r");
	$line = fgets($handle);
	if (trim($line) == 'y' or trim($line) == 'yes') {
		unlink($file);
	} else {
		echo "\033[31mDo nothing\033[0m \n";
		exit;	
	}
}

function dump($file) {
	if (file_exists($file)) {
		echo "\033[31mFile ".$file." exist  \033[0m \n";
		echo "\033[32mOld file now ".update_file_name($file)."\033[0m \n";
		echo mysqldump($file);
		echo "\033[1;32mFile ".$file." created. Filesize = ".FileSizeConvert(filesize($file))."\033[0m \n";
	} else {
		echo "\033[32mFile ".$file." not exist\033[0m \n";
		echo mysqldump($file);
		echo "\033[1;32mFile ".$file." created. Filesize = ".FileSizeConvert(filesize($file))."\033[0m \n";
	}
}

function restore($file) {
	if (file_exists($file)) {
		echo "\033[32mFile ".$file." exist. Filesize = ".FileSizeConvert(filesize($file))."\033[0m \n";
		echo mysqlupload($file);
		remove($file);
	} else {
		echo "\033[31mCan't find ".$file."!\033[0m \n";
		if (glob("*.sql*")) {
			echo "\033[32mYou can try another .sql!\033[0m \n";
			foreach (glob("*.sql*") as $filename) {
				echo "$filename filesize " . FileSizeConvert(filesize($filename)) . "\n";
			}
			echo "\033[32mWrite file name\033[0m \n";
			$handle = fopen ("php://stdin","r");
			$line = fgets($handle);
			if (file_exists(trim($line))) {
				echo mysqlupload(trim($line));
				remove(trim($line));
			} else {
				echo "\033[31mExit\033[0m \n";
				exit;	
			}
		}
	}
}

if (!@$argv[1]) {
	echo "\033[35mNo arguments!\nWrite 'dump' for backup current db \nWrite 'res' for upload dump.sql to db\033[0m \n";
	$handle = fopen ("php://stdin","r");
	$line = fgets($handle);
	if (trim($line) == 'dump' or trim($line) == 'вгьз') {
		echo "\033[36mGo dump\033[0m \n";
		dump($dumpname);
	} elseif (trim($line) == 'res' or trim($line) == 'куы') {
		echo "\033[36mGo restore\033[0m \n";
		restore($dumpname);
	} else {
		echo "\033[31mABORTING!\033[0m \n";
		exit;	
	}
} elseif ($argv[1] == 'dump') {
	echo "\033[36mGo dump\033[0m \n";
	dump($dumpname);
} elseif ($argv[1] == 'res') {
	echo "\033[36mGo restore\033[0m \n";
	restore($dumpname);
} else {
	echo "\033[35mWrong arguments!\n'dump' for backup current db \n'res' for upload dump.sql to db\033[0m \n";
}
?>